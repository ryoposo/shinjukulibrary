<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/h1design.css">
	<link rel="stylesheet" type="text/css" href="../css/form.css">
	<link rel="stylesheet" type="text/css" href="../css/button.css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript">
	$(function(){
		$("#header").load("../common/header.html");
	})
	</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>資料返却</title>
</head>
<body>
	<div id="header"></div>
	<h1>資料返却</h1>
	<form action="form" method="get">
	<table align="center">
		<tr>
			<td class="item"><b>会員ID</b></td><td><input type="text" name="id" size="20" value="${memberid}" pattern="^[0-9]+$" maxlength="9"><input type="submit" value="確認"></td>
		</tr>
	</table>
	</form>

	<c:if test="${num != 0}">
		<form action="form" method="post">
		<input type="hidden" name="memberid" value="${memberid}">
		<table align="center">
			<thead>
				<tr>
					<th></th>
					<th>ID</th>
					<th>タイトル</th>
					<th>返却期限</th>
				</tr>
			</thead>
			<c:forEach items="${rentals}" var="rental">
				<tr>
					<td width="50"><input type="checkbox" name="check" value="${rental.documentId}"></td>
					<td>${rental.documentId}</td>
					<td>${rental.documentTitle}</td>
					<td>${rental.limitDate}</td>
				</tr>
			</c:forEach>
		</table>
		<div class="form"><button type="submit">送信</button></div>
		</form>
	</c:if>

</body>
</html>