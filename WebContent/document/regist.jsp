<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/h1design.css">
	<link rel="stylesheet" type="text/css" href="../css/form.css">
	<link rel="stylesheet" type="text/css" href="../css/button.css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript">
	$(function(){
		$("#header").load("../common/header.html");
	})
	</script>
	<script type="text/javascript" src="../javascript/isbn_check_regist.js"></script>
	<script type="text/javascript" src="../javascript/check_btn_regist.js"></script>
	<script type="text/javascript" src="../javascript/change_btn_regist.js"></script>
	<title>資料登録</title>
</head>
<body>
	<div id="header"></div>
	<h1>新規資料登録</h1>
	<form action="" method="post" id="document_form">
		<table align="center">
			<tr>
				<td class="item"><b>ISBN</b></td><td><input type="text" name="isbn" id="check_isbn1" size="20" maxlength="20" >
				 <input type="button" value="check" id="check_btn1" onclick="cd_check();">
				 <input type="button" value="change" id="change_btn1" onclick="cd_check();" disabled>
				 </td>
			</tr>
			<tr>
				<td class="item"><b>資料名</b></td><td><input type="text" name="title" size="20" required></td>
			</tr>
			<tr>
				<td class="item"><b>分類コード</b></td>
				<td>
					<select name="category_code" size="1">
						<c:forEach items="${cat}" var="ca">
							<option value="${ca.categoryCode}">${ca.categoryName}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td class="item"><b>著者名</b></td><td><input type="text" name="author" required></td>
			</tr>
			<tr>
				<td class="item"><b>出版社</b></td><td><input type="text" name="publisher" required></td>
			</tr>
			<tr>
				<td class="item"><b>出版日</b></td><td><input type="date" name="publish_date" required pattern="\d{4}-\d{2}-\d{2}"></td>
			</tr>
			<tr>
				<td class="item"><b>資料追加数</b></td>
				<td><select name="number_of_books" size="1">
					<option value="1" selected>1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					</select>
			</tr>
		</table>
		<br>
		<div class="form"><button type="submit" id="btn1" disabled>登録</button></div>
	</form>
</body>
</html>