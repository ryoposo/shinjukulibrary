<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/h1design.css">
	<link rel="stylesheet" type="text/css" href="../css/tabstyle.css">
	<link rel="stylesheet" type="text/css" href="../css/button.css">
	<link rel="stylesheet" type="text/css" href="../css/info.css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript">
	$(function(){
		$("#header").load("../common/header.html");
	})

	function changeTab(tabname) {
		//全部けすよ
		document.getElementById('library').style.display = 'none';
		document.getElementById('document').style.display = 'none';
		//指定タブだけ表示するよ
		document.getElementById(tabname).style.display = 'block';
	}

	</script>
	<script type="text/javascript" src="../javascript/isbn_check.js"></script>
	<script type="text/javascript" src="../javascript/check_btn.js"></script>
	<script type="text/javascript" src="../javascript/change_btn.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>資料情報-${library.id} ${document.title}</title>
</head>
<body>
	<div id="header"></div>
	<div class="tabbox">
		<p class="tabs">
			<a href="#library" class="library" onclick="changeTab('library'); return false;">所蔵資料修正</a>
			<a href="#document" class="document" onclick="changeTab('document'); return false;">資料修正</a>
		</p>
		<div id="library" class="tab">
		<form action="../library/update" method="post" id="document_form">
			<input type="hidden" name="id" value="${library.id}">
			<table align="center">
				<tr>
					<td class="item"><b>資料ID</b><td>${library.id}</td>
				</tr>
				<tr>
					<td class="item"><b>ISBN</b><td><input type="text" name="isbn" id="check_isbn" value="${library.isbn}">
					<input type="button" value="check" id="check_btn" onclick="cd_check();">
					<input type="button" value="change" id="change_btn" onclick="cd_check();" disabled>
					</td>
				</tr>
				<tr>
					<td class="item"><b>入荷年月日</b><td><input type="date" name="arriveDate" value="${library.arriveDate}" pattern="\d{4}-\d{2}-\d{2}"></td>
				</tr>
				<tr>
					<td class="item"><b>破棄年月日</b><td><input type="date" name="nullifyDate" value="${library.nullifyDate}" pattern="\d{4}-\d{2}-\d{2}"></td>
				</tr>
				<tr>
					<td class="item"><b>備考</b><td><textarea name="note" cols=40 rows=4>${library.note}</textarea></td>
				</tr>
			</table>
			<br>
			<div class="form"><button id="btn" type="submit" disabled>変更</button></div>
		</form>
		<form action="../library/delete" method="post">
			<input type="hidden" name="id" value="${library.id}">
			<div class="form"><button type="submit" class="red"onclick='return confirm("この資料を破棄してもよろしいですか？")'>資料を破棄</button></div>
		</form>
		</div>
		<div id="document" class="tab">
		<form action="../document/update" method="post">
			<input type="hidden" name="isbn" value="${document.isbn}">
			<table align="center">
				<tr>
					<td class="item"><b>ISBN</b><td>${document.isbn}</td>
				</tr>
				<tr>
					<td class="item"><b>資料名</b><td><input type="text" name="title" value="${document.title}"></td>
				</tr>
				<tr>
				<td class="item"><b>分類コード</b></td>
				<td>
					<select name="categoryCode" size="1">
						<c:forEach items="${cat}" var="ca">
							<c:choose>
								<c:when test="${ca.categoryCode == document.categoryCode }">
									<option value="${ca.categoryCode}" selected>${ca.categoryName}</option>
								</c:when>
								<c:otherwise>
									<option value="${ca.categoryCode}">${ca.categoryName}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</td>
				</tr>
				<tr>
					<td class="item"><b>著者名</b><td><input type="text" name="author" value="${document.author}"></td>
				</tr>
				<tr>
					<td class="item"><b>出版社</b><td><input type="text" name="publisher" value="${document.publisher}"></td>
				</tr>
				<tr>
					<td class="item"><b>出版日</b><td><input type="date" name="publishDate" value="${document.publishDate}" pattern="\d{4}-\d{2}-\d{2}"></td>
				</tr>
			</table>
			<br>
			<div class="form"><button type="submit">変更</button></div>

		</form>
		</div>
	</div>
	<script type="text/javascript">
		changeTab('library');
	</script>
</body>
</html>