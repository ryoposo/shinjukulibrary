<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/h1design.css">
	<link rel="stylesheet" type="text/css" href="../css/search.css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript">
	$(function(){
		$("#header").load("../common/header.html");
	})

	var postForm = function(url, data) {
        var $form = $('<form/>', {'action': url, 'method': 'post'});
        for(var key in data) {
                $form.append($('<input/>', {'type': 'hidden', 'name': key, 'value': data[key]}));
        }
        $form.appendTo(document.body);
        $form.submit();
	};

	jQuery(function($) {
	  $('tr[data-id]').addClass('clickable')
	    .click(function(e) {
	      if(!$(e.target).is('a')){
	      	//window.location = $(e.target).closest('tr').data('href');
	      	var data = {id:$(e.target).closest('tr').data('id')};
	      	postForm('info', data);
	      };
	  });
	});
	</script>
	<title>資料検索</title>
</head>
<body>
	<div id="header"></div>
	<h1>資料検索</h1>
		<form action="search" method="get">
		<input type="radio" name="option" value="title" checked>資料名
		<input type="radio" name="option" value="isbn">ISBN
		<input type="text" name="key" size="20"><input type="submit" value="検索">
	</form><br>
	<table class="search_result">
		<thead>
			<tr>
				<th>ID</th>
				<th>ISBN</th>
				<th>資料名</th>
				<th>分類コード</th>
				<th>著者</th>
				<th>出版社</th>
				<th>出版年月日</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${results}" var="result">
				<tr data-id="${result.id}">
					<td>${result.id}</td>
					<td>${result.isbn}</td>
					<td>${result.title}</td>
					<td>${result.categoryCode}</td>
					<td>${result.author}</td>
					<td>${result.publisher}</td>
					<td>${result.publishDate}</td>
				</tr>
			</c:forEach>
		</tbody>

	</table>
</body>
</html>