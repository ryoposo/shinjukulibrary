<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/h1design.css">
	<link rel="stylesheet" type="text/css" href="../css/button.css">
	<link rel="stylesheet" type="text/css" href="../css/info.css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript">
	$(function(){
		$("#header").load("../common/header.html");
	})
	</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>会員情報-${member.name}</title>
</head>
<body>
	<div id="header"></div>
	<h1>会員情報</h1>
	<form action="update" method="post">
		<input type="hidden" name="id" value="${member.id}">
		<table align="center">
			<tr>
				<td class="item"><b>会員ID</b></td><td>${member.id}</td>
			</tr>
			<tr>
				<td class="item"><b>氏名</b></td><td><input type="text" name="name" size="20" value="${member.name}"></td>
			</tr>
			<tr>
				<td class="item"><b>生年月日</b></td><td><input type="date" name="birth" size="20" value="${member.birth}" pattern="\d{4}-\d{2}-\d{2}"></td>
			</tr>
			<tr>
				<td class="item"><b>住所</b></td><td><input type="text" name="address" size="20" value="${member.address}"></td>
			</tr>
			<tr>
				<td class="item"><b>電話番号</b></td><td><input type="tel" name="tel" size="20" value="${member.tel}" pattern="^[0-9]+$"></td>
			</tr>
			<tr>
				<td class="item"><b>メールアドレス</b></td><td><input type="email" name="mail" size="20" value="${member.mail}"></td>
			</tr>
			<tr>
				<td class="item"><b>退会年月日</b></td><td>${member.leaveDate}</td>
			</tr>
		</table>
		<br>
		<div class="form"><button type="submit">変更</button></div>
	</form>
	<c:if test="${fn:length(member.leaveDate) == 0}">
		<form action="leave" method="post">
			<input type="hidden" name="id" value="${member.id}">
			<div class="form"><button type="submit" class="red" onclick='return confirm("このユーザーを退会させてもよろしいですか？")'>ユーザーを退会</button></div>
		</form>
	</c:if>
</body>
</html>