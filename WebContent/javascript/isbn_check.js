function cd_check() {
	//フォームの値ゲットだぜ!w7
	if(document.getElementById("check_btn").disabled == true){
		change_btn();
	}
	else{
		check=false;
		s = document.forms.document_form.check_isbn.value;


		//13桁かどうか調べるよ
		if (s.length == 13){
			set = 1;
		}
		else if(s.length == 10){
			set = 2;
		}
		else{
			set = 0;
		}

		//13桁の場合だよ
		if (set == 1) {
			//チェックディジットとってくるよ
			cd = s.substr(s.length-1,1);
			//初期化
			m = 0;
			t1 = 0;
			t2 = 0;
			//偶数桁奇数桁の足し算
			for (i=0;i<12;i++) {
				if (m == 0) {
					//奇数桁６つを足すよ
					t1 = t1 + parseInt(s.substr(i,1));
					m = 1;
				} else {
					//偶数桁６つを足すよ
					t2 = t2 + parseInt(s.substr(i,1));
					m = 0;
				}
			}
			//奇数桁と偶数桁の３倍値を足すよ
			m = t1 + t2 * 3;
			//mを文字列にするよ
			s = "" + m;
			//加算値の１桁目を10から引くよ
			m = 10 - parseInt(s.substr(s.length-1,1));
			if (m == 10) {m = 0;}

			//表示
			if (cd == m){
				alert("正しいISBN番号です。");
				check=true;
				check_btn(check);
			}
			else{
				alert("間違ったISBN番号です。");
				check=false;
				check_btn(check);
			}

		}
		//10桁の場合だよ
		else if(set == 2){
			//チェックディジットとってくるよ
			cd = s.substr(s.length-1,1);
			j=10;
			sum=0;
			//めんどっちぃ計算だよ
			for(i=0; i<9; i++){
				sum += parseInt(s.substr(i,1))*j;
				j--;
			}
			n = sum%11;
			m = 11-n;

			//表示
			if (cd == m){
				alert("正しいISBN番号です。");
				check=true;
				check_btn(check);
			}
			else{
				alert("間違ったISBN番号です。");
				check=false;
				check_btn(check);
			}
		}
		//桁数ちげぇよ
		else {
			//表示
			alert("13桁か10桁で入力してください。");
			check=false;
			check_btn(check);
		}
	}
}