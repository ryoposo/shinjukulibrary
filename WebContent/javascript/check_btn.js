function check_btn(check){

  //名前若しくは感想欄のどちらかが空かチェック
  if (check == true)
  {
    document.getElementById("btn").disabled = "";
    document.getElementById("check_isbn").readOnly = true;
    document.getElementById("change_btn").disabled = "";
    document.getElementById("check_btn").disabled = true;
    document.forms.document_form.check_isbn.style.backgroundColor = "#CCC";
  }else{
    document.getElementById("btn").disabled = true;
    document.getElementById("check_isbn").readOnly = "";
  }
}