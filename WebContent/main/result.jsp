<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../css/h1design.css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript">
	$(function(){
		$("#header").load("../common/header.html");
		$("#main").load("main.html");
	})
	</script>
<title>${pageTitle}</title>
</head>
<body>
	<div id="header"></div>
	${resultMessage}
	<p>
		<input type="button" onclick="location.href='../main/index.html'"value="トップへ戻る">
	</p>
</body>
</html>