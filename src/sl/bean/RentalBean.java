package sl.bean;
import java.io.Serializable;

public class RentalBean implements Serializable{

	private int	memberId;
	private int	documentId;
	private String	documentTitle;
	private String	rentalDate;
	private String	returnDate;
	private String	limitDate;
	private String	note;

	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	public int getDocumentId() {
		return documentId;
	}
	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}
	public String getDocumentTitle() {
		return documentTitle;
	}
	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}
	public String getRentalDate() {
		return rentalDate;
	}
	public void setRentalDate(String rentalDate) {
		this.rentalDate = rentalDate;
	}
	public String getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(String returnDate) {
		this.returnDate = returnDate;
	}
	public String getLimitDate() {
		return limitDate;
	}
	public void setLimitDate(String limitDate) {
		this.limitDate = limitDate;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}

	public RentalBean(){}
	public RentalBean(int memberId,int documentId,String rentalDate,String returnDate,String note){
		this.memberId = memberId;
		this.documentId =documentId;
		this.rentalDate = rentalDate;
		this.returnDate = returnDate;
		this.note = note ;
	}

}
