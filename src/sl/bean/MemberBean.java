package sl.bean;

import java.io.Serializable;

public class MemberBean implements Serializable{

	private int	id;
	private String	name;
	private String	address;
	private String	tel;
	private String	mail;
	private String	birth;
	private String	admitDate;
	private String	leaveDate;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public String getAdmitDate() {
		return admitDate;
	}
	public void setAdmitDate(String admitDate) {
		this.admitDate = admitDate;
	}
	public String getLeaveDate() {
		return leaveDate;
	}
	public void setLeaveDate(String leaveDate) {
		this.leaveDate = leaveDate;
	}

	public MemberBean(){ }
	public MemberBean(int id,String name,String address,String tel,String mail,String birth,String admitDate,String leaveDate){
		this.id = id;
		this.name = name;
		this.address = address;
		this.tel = tel;
		this.mail = mail;
		this.birth = birth;
		this.admitDate = admitDate;
		this.leaveDate = leaveDate;
	}

}
