package sl.bean;
import java.io.Serializable;

public class DocumentBean implements Serializable{

	private long isbn;
	private String title;
	private int categoryCode;
	private String author;
	private String publisher;
	private String publishDate;

	public long getIsbn() {
		return isbn;
	}

	public void setIsbn(long isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(int categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}

	public DocumentBean(){}
	public DocumentBean(long isbn,String title,int categoryCode,String auther,String publisher,String publishDate){
		this.isbn = isbn;
		this.title = title;
		this.categoryCode = categoryCode;
		this.author = auther;
		this.publisher = publisher;
		this.publishDate = publishDate;
	}

}
