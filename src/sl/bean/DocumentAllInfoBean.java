package sl.bean;

import java.io.Serializable;

public class DocumentAllInfoBean implements Serializable {

	private int id;
	private long isbn;
	private String title;
	private int categoryCode;
	private String author;
	private String publisher;
	private String publishDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getIsbn() {
		return isbn;
	}

	public void setIsbn(long isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(int categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}

	public DocumentAllInfoBean() {}
	public DocumentAllInfoBean(DocumentBean db, DocumentLibraryBean dlb) {
		setId(dlb.getId());
		setIsbn(dlb.getIsbn());
		setTitle(db.getTitle());
		setCategoryCode(db.getCategoryCode());
		setAuthor(db.getAuthor());
		setPublisher(db.getPublisher());
		setPublishDate(db.getPublishDate());
	}
}
