package sl.bean;
import java.io.Serializable;

public class DocumentLibraryBean implements Serializable{

	private int	id;
	private long	isbn;
	private String	arriveDate;
	private String	nullifyDate;
	private String	note;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getIsbn() {
		return isbn;
	}
	public void setIsbn(long isbn) {
		this.isbn = isbn;
	}
	public String getArriveDate() {
		return arriveDate;
	}
	public void setArriveDate(String arriveDate) {
		this.arriveDate = arriveDate;
	}
	public String getNullifyDate() {
		return nullifyDate;
	}
	public void setNullifyDate(String nullifyDate) {
		this.nullifyDate = nullifyDate;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}

	public DocumentLibraryBean(){}
	public DocumentLibraryBean(int id,long isbn,String arriveDate,String nullifyDate,String note){
		this.id = id;
		this.isbn = isbn;
		this.arriveDate = arriveDate;
		this.nullifyDate = nullifyDate;
		this.note = note;
	}




}
