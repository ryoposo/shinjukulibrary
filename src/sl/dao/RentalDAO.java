package sl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import sl.bean.RentalBean;
import sl.utility.DataBaseUtility;
import sl.utility.RentalResult;

public class RentalDAO {
	private static RentalDAO _instance;
	public static RentalDAO instance() {
		if(_instance == null) _instance = new RentalDAO();
		return _instance;
	}
	private RentalDAO() { _instance = null; }

	// -- for select message function
	//借りているものを調べるのよ
	public RentalBean[] getRentalByMemberId(int id) {
		RentalBean[] result = null;
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT r.document_id, d.title, r.limit_date FROM rental r JOIN library l ON r.document_id = l.document_id JOIN document d ON l.isbn = d.isbn WHERE (r.member_id = ? AND return_date IS NULL)";

			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			ArrayList<RentalBean> list = new ArrayList<RentalBean>();
			while(rs.next()){
				//create bean obj
				RentalBean bean = new RentalBean();
				bean.setDocumentId(rs.getInt("document_id"));
				bean.setDocumentTitle(rs.getString("title"));
				bean.setLimitDate(rs.getString("limit_date"));
				//add list to bean
				list.add(bean);
			}
			result = new RentalBean[list.size()];
			list.toArray(result);
			rs.close();
			st.close();

		}catch(SQLException e){
			e.printStackTrace();
			System.out.println("SQLエラー(貸出資料検索)");
		}
		return result;
	}

	public RentalBean getRentalByDocumentId(int id) {
		RentalBean result = null;
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT r.member_id, d.title, r.limit_date FROM rental r JOIN library l ON r.document_id = l.document_id JOIN document d ON l.isbn = d.isbn WHERE (r.document_id = ? AND return_date IS NULL)";

			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			while(rs.next()){
				//create bean obj
				result = new RentalBean();
				result.setMemberId(rs.getInt("member_id"));
				result.setDocumentTitle(rs.getString("title"));
				result.setLimitDate(rs.getString("limit_date"));
			}
			rs.close();
			st.close();

		}catch(SQLException e){
			e.printStackTrace();
			System.out.println("SQLエラー(貸出資料検索)");
		}
		return result;
	}

	//借りている個数を調べるだ
	public int getRentalNumber(int id) {
		int result = 0;
		try {
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT rent_document(?)";

			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			while(rs.next()) {
				result = rs.getInt(1);
			}
		}catch(Exception e) {
			e.printStackTrace();
			System.out.println("SQLエラー(貸出個数取り出し)");
		}
		return result;
	}

	// -- for insert message function
	//貸出日を追加して新しい貸出情報を登録するのら
	public RentalResult addNewRental(RentalBean rental) {
		RentalResult result = RentalResult.ALREADY_RENTALED;
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT rent(?, ?, ?)";
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, rental.getMemberId());
			st.setInt(2, rental.getDocumentId());
			st.setString(3, rental.getNote());
			ResultSet rs = st.executeQuery();
			while(rs.next()){
				if(rs.getInt("rent")==0){//貸出成功
					result = RentalResult.SUCCESS;
					System.out.println("貸出情報書き込み完了");
				}
				if(rs.getInt("rent")==1){//期限超過エラー
					result = RentalResult.NEED_RETURN;
					System.out.println("資料返せ");
				}
				if(rs.getInt("rent")==2){//退会会員エラー
					result = RentalResult.MEMBER_LEAVED;
					System.out.println("退会している会員です");
				}
				if(rs.getInt("rent")==3){//既に貸している資料だよエラー
					result = RentalResult.ALREADY_RENTALED;
					System.out.println("貸出中資料です");
				}
			}
			rs.close();
			st.close();
		}catch(SQLException e){
			e.printStackTrace();
			System.out.println("SQLエラー(資料登録)");
		}
		return result;
	}

	//返却日を追加するのらｗ
	public void addReturnDate(int memberid, int[] docids) {
		for(int docid : docids) {
			//ここから先にDBへの保存処理をおなしゃす 05/23 hiroishi.
			try{
				Connection con = DataBaseUtility.getConnection();
				String sql = "SELECT return(?, ?)";
				PreparedStatement st = con.prepareStatement(sql);
				st.setInt(1, memberid);
				st.setInt(2, docid);
				ResultSet rs = st.executeQuery();
				while(rs.next()){
				}
				System.out.println("返却完了");
				rs.close();
				st.close();
			}catch(SQLException e){}
		}
	}
}
