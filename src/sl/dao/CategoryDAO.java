package sl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import sl.bean.CategoryBean;
import sl.utility.DataBaseUtility;

public class CategoryDAO {
	private static CategoryDAO _instance;
	public static CategoryDAO instance() {
		if(_instance == null) _instance = new CategoryDAO();
		return _instance;
	}
	private CategoryDAO() { _instance = null; }

	// -- for select message function
	public CategoryBean[] getAllCategory() {
		CategoryBean[] result = null;
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT * FROM document_category";
			PreparedStatement st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			ArrayList<CategoryBean> list = new ArrayList<CategoryBean>();
			while(rs.next()){
				CategoryBean category = new CategoryBean(rs.getInt("category_code"), rs.getString("category_name"));
				list.add(category);
			}
			result = new CategoryBean[list.size()];
			list.toArray(result);
			rs.close();
			st.close();

		}catch(SQLException e){
			System.out.println("SQLエラー(カテゴリー一覧取得)");
		}
		return result;

	}

	public CategoryBean getCategory(int id) {
		CategoryBean result = null;
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT * FROM document_category WHERE category_code = ?";
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			while(rs.next()) {
				result = new CategoryBean(rs.getInt("category_code"), rs.getString("category_name"));
			}
			rs.close();
			st.close();

		}catch(SQLException e){
			System.out.println("SQLエラー(特定のカテゴリー取得)");
		}
		return result;
	}
}
