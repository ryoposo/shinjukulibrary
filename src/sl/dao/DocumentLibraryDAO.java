package sl.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import sl.bean.DocumentLibraryBean;
import sl.utility.DataBaseUtility;

public class DocumentLibraryDAO {
	private static DocumentLibraryDAO _instance;
	public static DocumentLibraryDAO instance() {
		if(_instance == null) _instance = new DocumentLibraryDAO();
		return _instance;
	}
	private DocumentLibraryDAO() { _instance = null; }

	// -- for select message functions
	//IDから検索するよ
	public DocumentLibraryBean getDocumentLibraryById(int id) {
		DocumentLibraryBean result = null;
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT * FROM library WHERE document_id = ?";
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();

			while(rs.next()){
				result = new DocumentLibraryBean(id, rs.getLong("isbn"), rs.getString("arrive_date"),
						rs.getString("nullify_date"), rs.getString("document_note"));
			}

			rs.close();
			st.close();

		}catch(SQLException e){}
		return result;
	}
	//isbnから検索するよ
	public DocumentLibraryBean[] getDocumentLibraryByIsbn(long isbn) {
		DocumentLibraryBean result[] = null;
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT * FROM library WHERE isbn = ?";
			PreparedStatement st = con.prepareStatement(sql);
			st.setLong(1, isbn);
			ResultSet rs = st.executeQuery();
			ArrayList<DocumentLibraryBean> list = new ArrayList<DocumentLibraryBean>();
			while(rs.next()){
				//create bean obj
				DocumentLibraryBean bean = new DocumentLibraryBean();
				bean.setId(rs.getInt("document_id"));
				bean.setIsbn(rs.getLong("isbn"));
				bean.setArriveDate(rs.getString("arrive_date"));
				bean.setNullifyDate(rs.getString("nullify_date"));
				bean.setNote(rs.getString("document_note"));
				//add list to bean
				list.add(bean);
			}
			result = new DocumentLibraryBean[list.size()];
			list.toArray(result);
			rs.close();
			st.close();

		}catch(SQLException e){
			e.printStackTrace();
		}
		return result;
	}

	// -- for insert message functions
	//新規ドキュメントを追加するよ
	public void addDocumentLibrary(DocumentLibraryBean library) {
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "INSERT INTO library VALUES (?, ?, ?, ?, ?)";
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, library.getId());
			st.setLong(2, library.getIsbn());

			java.sql.Date arrive_date = Date.valueOf(library.getArriveDate());
			st.setDate(3, arrive_date);

			if(library.getNullifyDate()==null){
				st.setDate(4, null);
			}
			else{
				java.sql.Date nullify_date = Date.valueOf(library.getNullifyDate());
				st.setDate(4, nullify_date);
			}

			st.setString(5, library.getNote());
			st.executeUpdate();
			System.out.println("新規ドキュメント追加したよ");
			st.close();
		}catch(SQLException e){}

	}

	// -- for update message functions
	//ドキュメントの情報を更新するよ
	public void updateDocumentLibrary(DocumentLibraryBean library) {
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT update_library(?, ?, ?, ?, ?)";
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, library.getId());
			st.setLong(2, library.getIsbn());

			java.sql.Date arrive_date = Date.valueOf(library.getArriveDate());
			st.setDate(3, arrive_date);

			if(library.getNullifyDate()==null || library.getNullifyDate().isEmpty()){
				st.setDate(4, null);
			}
			else{
				java.sql.Date nullify_date = Date.valueOf(library.getNullifyDate());
				st.setDate(4, nullify_date);
			}
			st.setString(5, library.getNote());
			ResultSet rs = st.executeQuery();
			while(rs.next()){
				System.out.println("資料情報変更したよ");
			}
			rs.close();
			st.close();
		}catch(SQLException e){}

	}

	//ドキュメントに破棄情報を付与するよ
	public void disposeDocumentLibrary(int id) {
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT delete_document(?)";
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			while(rs.next()){

			}
			System.out.println("廃棄年月日入れたよ");
			rs.close();
			st.close();
		}catch(SQLException e){}
	}

	// -- for other functions
	//次のIDを取得するよ
	public int getNextId() {
		//いらないっぽいよ byはげ
		return -1;
	}
}
