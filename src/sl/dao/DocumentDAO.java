package sl.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import sl.bean.DocumentBean;
import sl.utility.DataBaseUtility;

public class DocumentDAO {
	private static DocumentDAO _instance;
	public static DocumentDAO instance() {
		if(_instance == null) _instance = new DocumentDAO();
		return _instance;
	}
	private DocumentDAO() { _instance = null; }

	// -- for select message functions
	//ISBNから検索するよ(ISBN検索であれば検索結果が1件に絞れる)
	public DocumentBean getDocumentByISBN(long isbn) {
		DocumentBean result = null;
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT * FROM document WHERE isbn = ?";
			PreparedStatement st = con.prepareStatement(sql);
			st.setLong(1, isbn);
			ResultSet rs = st.executeQuery();

			while(rs.next()){
				result = new DocumentBean(rs.getLong("isbn"), rs.getString("title"), rs.getInt("category_code"),
									rs.getString("author"),rs.getString("publisher"),rs.getString("publish_date"));
			}
			rs.close();
			st.close();

		}catch(SQLException e){
			e.printStackTrace();
			System.out.println("SQLエラー(ISBN検索)");
		}
		return result;
	}

	//タイトルから検索するよ(タイトル検索はあいまい検索があるので複数の検索結果が出る)
	public DocumentBean[] getDocumentByTitle(String title) {
		DocumentBean[] result = null;
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT * FROM document WHERE title LIKE ?";
			PreparedStatement st = con.prepareStatement(sql);
			String search_title = "%" + title + "%";
			st.setString(1, search_title);
			ResultSet rs = st.executeQuery();

			ArrayList<DocumentBean> list = new ArrayList<DocumentBean>();

			while(rs.next()){
				//create bean obj
				DocumentBean bean = new DocumentBean();
				bean.setIsbn(rs.getLong("isbn"));
				bean.setTitle(rs.getString("title"));
				bean.setCategoryCode(rs.getInt("category_code"));
				bean.setAuthor(rs.getString("author"));
				bean.setPublisher(rs.getString("publisher"));
				bean.setPublishDate(rs.getString("publish_date"));

				//add list to bean
				list.add(bean);

			}
			result = new DocumentBean[list.size()];
			list.toArray(result);
		}catch(SQLException e){
			e.printStackTrace();
			System.out.println("SQLエラー(タイトル検索)");
		}
		return result;
	}

	// -- for insert message functions
	//新規ドキュメントを追加するよ
	public boolean addDocument(DocumentBean document) {
		boolean result = false;
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT insert_document(?, ?, ?, ?, ?, ?)";
			PreparedStatement st = con.prepareStatement(sql);
			st.setLong(1, document.getIsbn());
			st.setString(2, document.getTitle());
			st.setInt(3, document.getCategoryCode());
			st.setString(4, document.getAuthor());
			st.setString(5, document.getPublisher());

			java.sql.Date date = Date.valueOf(document.getPublishDate());

			st.setDate(6, date);
			ResultSet rs = st.executeQuery();
			while(rs.next()){
				boolean check=false;
				check=rs.getBoolean("insert_document");
				if(check==true){
					result = true;
					System.out.println("データベース書き込み完了");
				}
				else{
					result = false;
					System.out.println("データベース書き込み失敗");
				}
			}

		}catch(SQLException e){
			e.printStackTrace();
			System.out.println("SQLエラー(資料登録)");
		}
		return result;
	}

	// -- for update message functions
	//情報を更新するよ
	public void updateDocument(DocumentBean document) {
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT update_document(?, ?, ?, ?, ?, ?)";
			PreparedStatement st = con.prepareStatement(sql);
			st.setLong(1, document.getIsbn());
			st.setString(2, document.getTitle());
			st.setInt(3, document.getCategoryCode());
			st.setString(4, document.getAuthor());
			st.setString(5, document.getPublisher());
			java.sql.Date publish_date = Date.valueOf(document.getPublishDate());
			st.setDate(6, publish_date);

			ResultSet rs = st.executeQuery();
			while(rs.next()){
				System.out.println("変更完了");
			}
		}catch(SQLException e){
			e.printStackTrace();
			System.out.println("SQLエラー(資料変更)");
		}
	}

	// -- for other functions
	//既に本が存在しているか調べるよ
	public boolean exisstDocument(String isbn) {
		boolean result = false;
		//いらないっぽいよ byはげ
		return result;
	}
}
