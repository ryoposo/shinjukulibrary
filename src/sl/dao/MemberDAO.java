package sl.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import sl.bean.MemberBean;
import sl.utility.DataBaseUtility;
import sl.utility.MemberRegistResult;

public class MemberDAO {
	// -- for singleton
	public static MemberDAO _instance;
	public static MemberDAO instance() {
		if(_instance == null) _instance = new MemberDAO();
		return _instance;
	}
	private MemberDAO () { _instance = null; }

	// -- for select message functions
	//会員IDから検索するよ
	public MemberBean getMemberById(int id) {
		MemberBean result = null;
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT * FROM member WHERE member_id = ? ORDER BY member_id";
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();

			while(rs.next()){
			result = new MemberBean(rs.getInt("member_id"), rs.getString("name"), rs.getString("address"),
					rs.getString("tel"), rs.getString("mail"), rs.getString("birth"),
					rs.getString("admit_date"), rs.getString("leave_date"));
			}

			rs.close();
			st.close();

		}catch(SQLException e){}
		return result;
	}

	//名前から検索するよ
	public MemberBean[] getMemberByName(String name) {
		MemberBean[] result = null;
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "select * from member where name LIKE ? ORDER BY member_id";
			PreparedStatement st = con.prepareStatement(sql);
			String search_name = "%" + name + "%";
			st.setString(1, search_name);
			ResultSet rs = st.executeQuery();

			ArrayList<MemberBean> list = new ArrayList<MemberBean>();

			while(rs.next()){
				//create bean obj
				MemberBean bean = new MemberBean();
				bean.setId(rs.getInt("member_id"));
				bean.setName(rs.getString("name"));
				bean.setAddress(rs.getString("address"));
				bean.setTel(rs.getString("tel"));
				bean.setMail(rs.getString("mail"));
				bean.setBirth(rs.getString("birth"));
				bean.setAdmitDate(rs.getString("admit_date"));
				bean.setLeaveDate(rs.getString("leave_date"));

				//add list to bean
				list.add(bean);

			}
			result = new MemberBean[list.size()];
			list.toArray(result);
		}catch(SQLException e){}
		return result;
	}

	//メルアドから検索するよ
	public MemberBean getMemberByMailAddress(String mailAddress) {
		MemberBean result = null;
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "select * from member where mail = ? ORDER BY member_id";
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, mailAddress);
			ResultSet rs = st.executeQuery();

			while(rs.next()){
				result = new MemberBean(rs.getInt("member_id"), rs.getString("name"), rs.getString("address"),
						rs.getString("tel"), rs.getString("mail"), rs.getString("birth"),
						rs.getString("admit_date"), rs.getString("leave_date"));
			}
			rs.close();
			st.close();

		}catch(SQLException e){}
		return result;
	}

	// -- for insert message functions
	//新しい会員情報を登録するよ
	public MemberRegistResult addNewMember(MemberBean member) {
		MemberRegistResult result = null;
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql ="SELECT insert_member(?, ?, ?, ?, ?)";
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, member.getName());
			java.sql.Date birth = Date.valueOf(member.getBirth());
			st.setDate(2, birth);
			st.setString(3, member.getAddress());
			st.setString(4, member.getTel());
			st.setString(5, member.getMail());

			ResultSet rs = st.executeQuery();
			while(rs.next()){
				if(rs.getInt("insert_member")==0){ //通常会員登録
					result = MemberRegistResult.SUCCESS;
					System.out.println("会員登録したぞ");
				}
				else if(rs.getInt("insert_member")==1){ //重複しとる
					result = MemberRegistResult.DUPLICATE;
					System.out.println("既存会員がいるぞ");
				}
				else{ //再登録
					result = MemberRegistResult.REREGIST;
					System.out.println("会員登録したぞ（再）");
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
			System.out.println("SQLエラー（会員登録失敗）");
		}
		return result;
	}

	// -- for update message functions
	//会員情報を更新するよ
	public void updateMember(MemberBean member) {
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "UPDATE member SET name = ?, address = ?,"
					+ " tel = ?, mail = ?, birth = ? WHERE  member_id = ?";

			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, member.getName());
			st.setString(2, member.getAddress());
			st.setString(3, member.getTel());
			st.setString(4, member.getMail());

			java.sql.Date birth = Date.valueOf(member.getBirth());
			st.setDate(5, birth);
			st.setInt(6, member.getId());
			st.executeUpdate();
			System.out.println("会員情報変更したぞ");
		}catch(SQLException e){}
	}

	//退会情報を追加するよ

	public boolean leaveMember(MemberBean member) {
		boolean result = false;

		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT delete_member(?)";
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, member.getId());
			st.executeQuery();
			boolean check=false;
			ResultSet rs = st.executeQuery();
			while(rs.next()){
				check=rs.getBoolean("delete_member");
				if(check==true){
					result = true;
					System.out.println("退会させたぞ");
					result = true;
				}
				else{
					result = false;
					System.out.println("そいつレンタルあるから退会できんぞ");
					result = false;
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
			System.out.println("SQLエラー（退会処理）");
		}
		return result;
	}

	// -- for other functions
	//次のIDを取得するよ
	public int getNextId() {
		//いらないっぽいよ byはげ
		return -1;
	}

	//会員の重複をチェックするよ
	public boolean existMember(MemberBean member) {
		boolean result = false;
		//いらないっぽいよ byはげ
		return result;
	}

	public boolean existMember(String name, String tel, String mailAddress) {
		boolean result = false;
		try{
			Connection con = DataBaseUtility.getConnection();
			String sql = "SELECT leave_date FROM member WHERE name = ? AND tel = ? AND mail = ?";
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, name);
			st.setString(2, tel);
			st.setString(3, mailAddress);
			ResultSet rs = st.executeQuery();
			String check_leave=null;
			while(rs.next()){
				check_leave = rs.getString("leave_date");
			}
			System.out.println(check_leave);

			if(check_leave==null){
				result = true;
			}

		}catch(SQLException e){}

		return result;
	}
}
