package sl.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sl.bean.DocumentLibraryBean;
import sl.model.library.LibraryAlter;

@WebServlet("/library/update")
public class LibraryUpdateServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		DocumentLibraryBean dlb = new DocumentLibraryBean();
		dlb.setId(Integer.parseInt(request.getParameter("id")));
		dlb.setIsbn(Long.parseLong(request.getParameter("isbn")));
		dlb.setArriveDate(request.getParameter("arriveDate"));
		dlb.setNullifyDate(request.getParameter("nullifyDate"));
		dlb.setNote(request.getParameter("note"));
		LibraryAlter alter = new LibraryAlter(dlb);
		alter.execute();

		setResult(request);
		RequestDispatcher rd = request.getRequestDispatcher("../main/result.jsp");
		rd.forward(request, response);
	}

	private void setResult(HttpServletRequest request) {
		request.setAttribute("pageTitle", "資料情報変更完了");
		request.setAttribute("resultMessage", "資料情報を変更しました");
	}

}
