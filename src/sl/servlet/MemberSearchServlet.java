package sl.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sl.bean.MemberBean;
import sl.model.member.MemberCollector;
import sl.utility.SLUtility;

@WebServlet("/member/search")
public class MemberSearchServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String key = request.getParameter("key");
		String option = request.getParameter("option");

		//検索結果を格納する
		if(key != null) {
			//initialize
			MemberCollector collector = new MemberCollector();
			MemberBean[] results = null;
			if(option.equals(SLUtility.SEARCH_OP_NAME)) {
				//名前で検索
				results = collector.searchByName(key);
			}else if(option.equals(SLUtility.SEARCH_OP_ID)) {
				//IDで検索
				results = new MemberBean[1];
				try {
					results[0] = collector.searchById(Integer.parseInt(key));
				}catch(Exception e) {
					System.out.println("IDに数字以外が入力されたね");
				}
			}else if(option.equals(SLUtility.SEARCH_OP_MAIL)){
				//めるあどで検索
				results = new MemberBean[1];
				try {
					results[0] = collector.searchByMailAddress(key);
				}catch(Exception e) {}
			}
			//結果があった場合はせっとあとりびゅーとするのら
			if(results.length > 0) request.setAttribute("results", results);
		}
		RequestDispatcher rd = request.getRequestDispatcher("/member/search.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
