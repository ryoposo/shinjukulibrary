package sl.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sl.bean.MemberBean;
import sl.model.member.MemberRegister;
import sl.utility.MemberRegistResult;

/**
 * Servlet implementation class MemberRegistServlet
 */
@WebServlet("/member/regist")
public class MemberRegistServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		MemberBean member = new MemberBean();
		member.setName(request.getParameter("name"));
		member.setBirth(request.getParameter("birth"));
		member.setAddress(request.getParameter("address"));
		member.setTel(request.getParameter("tel"));
		member.setMail(request.getParameter("mail"));
		//登録するのら
		MemberRegister register = new MemberRegister(member);
		MemberRegistResult result = register.regist();

		//処理結果を表示させる
		switch(result) {
			case SUCCESS:
				setResultSuccess(request);
				break;
			case DUPLICATE:
				setResultDuplicate(request);
				break;
			case REREGIST:
				setResultReregist(request);
				break;
		}
		RequestDispatcher rd = request.getRequestDispatcher("../main/result.jsp");
		rd.forward(request, response);
	}

	private void setResultSuccess(HttpServletRequest request) {
		request.setAttribute("pageTitle", "会員登録完了");
		request.setAttribute("resultMessage", "会員登録が完了しました");
	}

	private void setResultDuplicate(HttpServletRequest request) {
		request.setAttribute("pageTitle", "会員登録失敗");
		request.setAttribute("resultMessage", "既に会員が存在します");
	}

	private void setResultReregist(HttpServletRequest request) {
		request.setAttribute("pageTitle", "会員登録成功");
		request.setAttribute("resultMessage", "既に存在している会員を再登録しました");
	}

}
