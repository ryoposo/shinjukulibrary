package sl.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sl.bean.CategoryBean;
import sl.bean.DocumentBean;
import sl.bean.DocumentLibraryBean;
import sl.dao.CategoryDAO;
import sl.model.document.DocumentCollector;
import sl.model.library.LibraryCollector;

@WebServlet("/document/info")
public class DocumentInfoServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		int id = Integer.parseInt(request.getParameter("id"));
		//かてごりだね
		CategoryBean[] cats = CategoryDAO.instance().getAllCategory();

		//ライブラリのほうだよ
		LibraryCollector lc = new LibraryCollector();
		DocumentLibraryBean dlb = lc.searchById(id);

		//資料のほうだよ
		DocumentCollector dc = new DocumentCollector();
		DocumentBean document = dc.searchByISBN(dlb.getIsbn());

		request.setAttribute("cat", cats);
		request.setAttribute("library", dlb);
		request.setAttribute("document", document);
		RequestDispatcher rd = request.getRequestDispatcher("../document/info.jsp");
		rd.forward(request, response);
	}

}
