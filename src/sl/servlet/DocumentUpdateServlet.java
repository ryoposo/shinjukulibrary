package sl.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sl.bean.DocumentBean;
import sl.model.document.DocumentAlter;

@WebServlet("/document/update")
public class DocumentUpdateServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		DocumentBean document = new DocumentBean();
		document.setIsbn(Long.parseLong(request.getParameter("isbn")));
		document.setTitle(request.getParameter("title"));
		document.setCategoryCode(Integer.parseInt(request.getParameter("categoryCode")));
		document.setAuthor(request.getParameter("author"));
		document.setPublisher(request.getParameter("publisher"));
		document.setPublishDate(request.getParameter("publishDate"));

		DocumentAlter alter = new DocumentAlter(document);
		alter.execute();

		setResult(request);
		RequestDispatcher rd = request.getRequestDispatcher("../main/result.jsp");
		rd.forward(request, response);
	}

	private void setResult(HttpServletRequest request) {
		request.setAttribute("pageTitle", "資料情報変更完了");
		request.setAttribute("resultMessage", "資料情報を変更しました");
	}


}
