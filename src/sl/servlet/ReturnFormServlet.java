package sl.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sl.bean.RentalBean;
import sl.model.member.MemberCollector;
import sl.model.rental.DocumentRentalAlter;
import sl.model.rental.DocumentRentalCollector;

/**
 * Servlet implementation class ReturnFormServlet
 */
@WebServlet("/return/form")
public class ReturnFormServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		request.setAttribute("memberid", id);
		MemberCollector collector = new MemberCollector();
		//collector.　メンバーは存在する？？
		if(id != null && !id.isEmpty()) {
			int id_num = Integer.parseInt(id);
			DocumentRentalCollector drc = new DocumentRentalCollector();
			RentalBean[] rent = drc.searchByMemberId(id_num);
			request.setAttribute("rentals", rent);
			request.setAttribute("num", rent.length); //貸し出している個数
		}else{
			request.setAttribute("num", 0);
		}
		RequestDispatcher rd = request.getRequestDispatcher("form.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int memberid = Integer.parseInt(request.getParameter("memberid"));
		String[] checkboxs = request.getParameterValues("check");
		int[] ids = new int[checkboxs.length];

		for(int i = 0;  i < ids.length; i++) {
			ids[i] = Integer.parseInt(checkboxs[i]);
		}

		//返却日を書くよ
		DocumentRentalAlter dra = new DocumentRentalAlter(memberid, ids);
		dra.writeReturnDate();

		setResult(request);
		RequestDispatcher rd = request.getRequestDispatcher("../main/result.jsp");
		rd.forward(request, response);
	}

	private void setResult(HttpServletRequest request) {
		request.setAttribute("pageTitle", "資料返却成功");
		request.setAttribute("resultMessage", "選択した資料を返却しました");
	}

}
