package sl.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sl.bean.CategoryBean;
import sl.bean.DocumentBean;
import sl.dao.CategoryDAO;
import sl.model.library.LibraryRegister;

/**
 * Servlet implementation class DocumentRegistServlet
 */
@WebServlet("/document/regist")
public class DocumentRegistServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CategoryBean[] cats =CategoryDAO.instance().getAllCategory();
		request.setAttribute("cat", cats);

		RequestDispatcher rd = request.getRequestDispatcher("/document/regist.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		long isbn = Long.parseLong(request.getParameter("isbn"));
		String title = request.getParameter("title");
		int categoryCode = Integer.parseInt(request.getParameter("category_code"));
		String author = request.getParameter("author");
		String publisher = request.getParameter("publisher");
		String publishDate = request.getParameter("publish_date");
		int numberOfBooks = Integer.parseInt(request.getParameter("number_of_books"));
		String note ="";
		DocumentBean doc = new DocumentBean(isbn, title, categoryCode, author, publisher, publishDate);
		LibraryRegister register = new LibraryRegister(doc, note);

		//ドキュメント登録の実行だよ
		if(register.regist(numberOfBooks)) {
			setResultSuccess(request);
		}else{
			setResultFailed(request);
		}
		RequestDispatcher rd = request.getRequestDispatcher("../main/result.jsp");
		rd.forward(request, response);
	}

	private void setResultSuccess(HttpServletRequest request) {
		request.setAttribute("pageTitle", "資料登録完了");
		request.setAttribute("resultMessage", "資料登録が完了しました");
	}

	private void setResultFailed(HttpServletRequest request) {
		request.setAttribute("pageTitle", "資料登録失敗");
		request.setAttribute("resultMessage", "資料登録に失敗しました");
	}
}
