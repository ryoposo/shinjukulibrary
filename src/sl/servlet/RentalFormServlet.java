package sl.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sl.model.member.MemberCollector;
import sl.model.rental.DocumentRentalCollector;
import sl.model.rental.DocumentRentalRegister;
import sl.utility.RentalResult;

@WebServlet("/rental/form")
public class RentalFormServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		request.setAttribute("memberid", id);

		MemberCollector collector = new MemberCollector();
		DocumentRentalCollector rc = new DocumentRentalCollector();

		//メンバーは存在する？？
		String status ="会員を検索してください";
		if(id != null && !id.isEmpty()) {
			int id_num = Integer.parseInt(id);
			int num = 0;
			if(collector.existMember(id_num)) {
				//退会している？
				if(collector.alreadyLeavedMember(id_num)) {
					//退会済み
					status = "既に退会している会員です";
				}else{
					//退会していない
					status = "会員が見つかりました";
					num = rc.getRentalNumberById(id_num);
				}
			}else{
				//会員が存在しない
				status = "会員が存在しません";
			}
			request.setAttribute("num", num); //貸出可能数
		}else{
			request.setAttribute("num", 0);
		}
		request.setAttribute("m_status", status); //貸出情報(貸せねえとか存在しねえとか)
		RequestDispatcher rd = request.getRequestDispatcher("form.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int memberid = Integer.parseInt(request.getParameter("memberid"));
		System.out.println(memberid);
		String[] docids = request.getParameterValues("docid");
		ArrayList<String> errorDocs = new ArrayList<String>();
		RentalResult result = null;
		for(String docid : docids) {
			if(docid.isEmpty() == false) {
				int id = Integer.parseInt(docid);
				//idで貸出処理をぶちこみたまえ
				DocumentRentalRegister drr = new DocumentRentalRegister(memberid, id);
				result = drr.execute();
				//未返却
				if(result == RentalResult.NEED_RETURN) {
					break;
				}
				//退会済み
				if(result == RentalResult.MEMBER_LEAVED) {
					System.out.println("おほー");
					break;
				}
				//既にレンタルされてるみたいだよ
				if(result == RentalResult.ALREADY_RENTALED) errorDocs.add(docid);
			}
		}

		if(result == RentalResult.NEED_RETURN) {
			setResultNeedReturn(request);
		}else if(result == RentalResult.MEMBER_LEAVED) {
			setResultMemberLeaved(request);
		}else {
			setResult(request, errorDocs);
		}
		RequestDispatcher rd = request.getRequestDispatcher("../main/result.jsp");
		rd.forward(request, response);

	}

	private void setResultNeedReturn(HttpServletRequest request) {
		request.setAttribute("pageTitle", "資料貸出結果");
		request.setAttribute("resultMessage", "貸出期限を超過している資料があるので貸し出せませんでした");
	}

	private void setResultMemberLeaved(HttpServletRequest request) {
		request.setAttribute("pageTitle", "資料貸出結果");
		request.setAttribute("resultMessage", "既に退会しているメンバーです");
	}

	private	void setResult(HttpServletRequest request, ArrayList<String> list) {
		request.setAttribute("pageTitle", "資料貸出結果");
		if(list.size() == 0) {
			request.setAttribute("resultMessage", "資料貸出登録が完了しました");
		}else{
			String all = "資料id:";
			for(String s : list) {
				all += s + ",";
			}
			all = all.substring(0, all.length() - 1); //後ろ1文字削除
			all += "を貸し出すことが出来ませんでした";
			request.setAttribute("resultMessage", all);
		}
	}

}
