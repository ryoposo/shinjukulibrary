package sl.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sl.bean.MemberBean;
import sl.model.member.MemberAlter;

@WebServlet("/member/update")
public class MemberUpdateServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		MemberBean member = new MemberBean();
		member.setId(Integer.parseInt(request.getParameter("id")));
		member.setName(request.getParameter("name"));
		member.setAddress(request.getParameter("address"));
		member.setTel(request.getParameter("tel"));
		member.setMail(request.getParameter("mail"));
		member.setBirth(request.getParameter("birth"));
		printRequestParam(member);
		//データベースに書き込む処理を記述せよ
		MemberAlter alter = new MemberAlter();
		alter.updateAll(member);
		setResult(request);
		RequestDispatcher rd = request.getRequestDispatcher("../main/result.jsp");
		rd.forward(request, response);
	}

	private void setResult(HttpServletRequest request) {
		request.setAttribute("pageTitle", "会員情報変更完了");
		request.setAttribute("resultMessage", "会員情報の変更が完了しました");
	}

	private void printRequestParam(MemberBean member) {
		System.out.println(member.getId());
		System.out.println(member.getName());
		System.out.println(member.getAddress());
		System.out.println(member.getTel());
		System.out.println(member.getMail());
		System.out.println(member.getBirth());
	}

}
