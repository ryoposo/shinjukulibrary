package sl.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sl.bean.MemberBean;
import sl.model.member.MemberCollector;

/**
 * Servlet implementation class MemberInfoServlet
 */
@WebServlet("/member/info")
public class MemberInfoServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//パラメータ取り出し
		int id = Integer.parseInt(request.getParameter("id"));
		//IDから情報検索
		MemberCollector collector = new MemberCollector();
		MemberBean member = collector.searchById(id);		request.setAttribute("member", member);
		//せっとあとりびゅーと
		request.setAttribute("member", member);
		//でぃすぱっち
		RequestDispatcher rd = request.getRequestDispatcher("../member/info.jsp");
		rd.forward(request, response);
	}

}
