package sl.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sl.model.library.LibraryRemover;

@WebServlet("/library/delete")
public class LibraryDeleteServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		LibraryRemover remover = new LibraryRemover(id);
		remover.execute();

		setResult(request);
		RequestDispatcher rd = request.getRequestDispatcher("../main/result.jsp");
		rd.forward(request, response);
	}

	private void setResult(HttpServletRequest request) {
		request.setAttribute("pageTitle", "資料破棄成功");
		request.setAttribute("resultMessage", "資料の破棄に成功しました");
	}

}
