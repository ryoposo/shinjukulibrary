package sl.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sl.bean.DocumentAllInfoBean;
import sl.model.library.LibraryCollector;
import sl.utility.SLUtility;

/**
 * Servlet implementation class DocumentSearchServlet
 */
@WebServlet("/document/search")
public class DocumentSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String key = request.getParameter("key");
		String option = request.getParameter("option");
		//検索
		if(key != null) {
			LibraryCollector collector = new LibraryCollector();
			//DocumentAllInfoBean[] results = collector.searchByTitle(key);
			DocumentAllInfoBean[] results = null;
			if(option.equals(SLUtility.SEARCH_OP_TITLE)) {
				//タイトルで検索
				results = collector.searchByTitle(key);
			}else if(option.equals(SLUtility.SEARCH_OP_ISBN)) {
				//ISBNで検索
				results = new DocumentAllInfoBean[1];
				try {
					results = collector.searchByIsbn(Long.parseLong(key));
				}catch(Exception e) {

				}
			}
			request.setAttribute("results", results);
		}
		RequestDispatcher rd = request.getRequestDispatcher("search.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
