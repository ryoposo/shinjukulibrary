package sl.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sl.model.member.MemberAlter;

@WebServlet("/member/leave")
public class MemberLeaveServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		//退会処理を実行
		MemberAlter alter = new MemberAlter();
		boolean result = alter.leave(id);

		if(result) {
			setResultSuccess(request);
		}else{
			setResultFailed(request);
		}
		RequestDispatcher rd = request.getRequestDispatcher("../main/result.jsp");
		rd.forward(request, response);
	}

	private void setResultSuccess(HttpServletRequest request) {
		request.setAttribute("pageTitle", "会員退会成功");
		request.setAttribute("resultMessage", "会員の退会に成功しました");
	}

	private void setResultFailed(HttpServletRequest request) {
		request.setAttribute("pageTitle", "会員退会失敗");
		request.setAttribute("resultMessage", "まだ貸出中の資料がある為、退会できません");
	}

}
