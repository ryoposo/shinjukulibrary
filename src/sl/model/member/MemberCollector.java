package sl.model.member;

import sl.bean.MemberBean;
import sl.dao.MemberDAO;

public class MemberCollector {

	//さがす
	public MemberBean searchById(int id) {
		MemberBean result = null;
		result = MemberDAO.instance().getMemberById(id);
		return result;
	}

	public MemberBean[] searchByName(String name) {
		MemberBean[] result = null;
		result = MemberDAO.instance().getMemberByName(name);
		return result;
	}

	public MemberBean searchByMailAddress(String mailAddress) {
		MemberBean result = null;
		result = MemberDAO.instance().getMemberByMailAddress(mailAddress);
		return result;
	}

	//存在している？
	public boolean existMember(int id) {
		return (MemberDAO.instance().getMemberById(id) != null);
	}

	//退会している？
	public boolean alreadyLeavedMember(int id) {
		MemberBean member = MemberDAO.instance().getMemberById(id);
		String date = member.getLeaveDate();
		return (date == null || date.isEmpty());
	}
}
