package sl.model.member;

import sl.bean.MemberBean;
import sl.dao.MemberDAO;
import sl.utility.DateUtility;

public class MemberAlter {

	public void updateAll(MemberBean member) {
		MemberDAO.instance().updateMember(member);
	}

	public boolean leave(int id) {
		MemberBean member = MemberDAO.instance().getMemberById(id);
		member.setLeaveDate(DateUtility.today());
		return MemberDAO.instance().leaveMember(member);
	}
}
