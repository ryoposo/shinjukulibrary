package sl.model.member;

import sl.bean.MemberBean;
import sl.dao.MemberDAO;
import sl.utility.MemberRegistResult;

public class MemberRegister {

	public MemberBean _member;

	public MemberRegister(MemberBean member) {
		_member = member;
	}

	//会員登録処理なのらｗ
	public MemberRegistResult regist() {
		MemberRegistResult result = null;
		//重複していない
		//重複してるけど退会している
//		boolean dup = checkDuplicate(_member);
//		if(dup == false || dup && checkLeave()) {
		result = MemberDAO.instance().addNewMember(_member);
//		}
		return result;
	}

	//重複確認するよｗ
	private boolean checkDuplicate(MemberBean member) {
		boolean result = false;
		result = MemberDAO.instance().existMember(member);
		return result;
	}

	//退会してんの？ｗ
	private boolean checkLeave() {
		boolean result = MemberDAO.instance().existMember("さいとう", "000000000", "adkaoskdoak@adadoa.com");
		return result;
	}
}
