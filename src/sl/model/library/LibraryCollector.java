package sl.model.library;

import java.util.ArrayList;

import sl.bean.DocumentAllInfoBean;
import sl.bean.DocumentBean;
import sl.bean.DocumentLibraryBean;
import sl.dao.DocumentDAO;
import sl.dao.DocumentLibraryDAO;

public class LibraryCollector {

	public DocumentLibraryBean searchById(int id) {
		return DocumentLibraryDAO.instance().getDocumentLibraryById(id);
	}

	public DocumentAllInfoBean[] searchByTitle(String title) {
		DocumentAllInfoBean[] result = null;
		//まずは該当する本を探してくるのらｗ
		DocumentBean[] docs = DocumentDAO.instance().getDocumentByTitle(title);

		//出てきた本を更にライブラリで検索して全部探すのらｗ
		ArrayList<DocumentAllInfoBean> list = new ArrayList<DocumentAllInfoBean>();
		for(DocumentBean doc : docs) {
			DocumentLibraryBean[] dlbs = DocumentLibraryDAO.instance().getDocumentLibraryByIsbn(doc.getIsbn());

			//リストにぶっこんでくるのらーｗ
			for(DocumentLibraryBean dlb : dlbs) {
				list.add(new DocumentAllInfoBean(doc, dlb));
			}
		}
		// ～～おわり～～
		result = new DocumentAllInfoBean[list.size()];
		list.toArray(result);
		return result;
	}

	public DocumentAllInfoBean[] searchByIsbn(long isbn) {
		DocumentAllInfoBean[] result = null;
		//まずは該当する本を探してくるのらｗ
		DocumentBean doc = DocumentDAO.instance().getDocumentByISBN(isbn);

		//出てきた本を更にライブラリで検索して全部探すのらｗ
		ArrayList<DocumentAllInfoBean> list = new ArrayList<DocumentAllInfoBean>();
		DocumentLibraryBean[] dlbs = DocumentLibraryDAO.instance().getDocumentLibraryByIsbn(doc.getIsbn());

			//リストにぶっこんでくるのらーｗ
			for(DocumentLibraryBean dlb : dlbs) {
				list.add(new DocumentAllInfoBean(doc, dlb));
			}
		// ～～おわり～～
		result = new DocumentAllInfoBean[list.size()];
		list.toArray(result);
		return result;
	}
}
