package sl.model.library;

import sl.bean.DocumentBean;
import sl.dao.DocumentDAO;

public class LibraryRegister {

	private DocumentBean _document;
	private String _note;

	public LibraryRegister(DocumentBean document, String note) {
		_document = document;
		_note = note;
	}

	//資料を追加する　まだ未完成だよ！！！！！DBサイドで必要なデータ知ったら作る！！5/23 hiroishi.
	// --!! 資料目録への追加はDBがやってくれてるらしいよ !!-- //
	public boolean regist(int numberOfBooks) {
		boolean result = false;
		for(int i=0; i<numberOfBooks; i++){
			result = DocumentDAO.instance().addDocument(_document);
		}
		return result;
	}
}
