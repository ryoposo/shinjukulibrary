package sl.model.library;

import sl.bean.DocumentLibraryBean;
import sl.dao.DocumentLibraryDAO;

public class LibraryAlter {

	private DocumentLibraryBean _documentLibrary;

	public LibraryAlter(DocumentLibraryBean dl) {
		_documentLibrary = dl;
	}

	public void execute() {
		DocumentLibraryDAO.instance().updateDocumentLibrary(_documentLibrary);
	}
}
