package sl.model.library;

import sl.dao.DocumentLibraryDAO;
import sl.utility.DateUtility;

public class LibraryRemover {

	private int _id;
	private String _date;

	public LibraryRemover(int id) {
		_id = id;
		_date = DateUtility.today();
	}

	public LibraryRemover(int id, String date) {
		_id = id;
		_date = date;
	}

	public void execute() {
		DocumentLibraryDAO.instance().disposeDocumentLibrary(_id);
	}
}
