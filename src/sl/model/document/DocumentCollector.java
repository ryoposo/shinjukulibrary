package sl.model.document;

import sl.bean.DocumentBean;
import sl.dao.DocumentDAO;

public class DocumentCollector {

	public DocumentBean searchByISBN(long isbn) {
		return DocumentDAO.instance().getDocumentByISBN(isbn);
	}
}
