package sl.model.document;

import sl.bean.DocumentBean;
import sl.dao.DocumentDAO;

public class DocumentAlter {

	private DocumentBean _document;

	public DocumentAlter(DocumentBean document) {
		_document = document;
	}

	public void execute() {
		DocumentDAO.instance().updateDocument(_document);
	}

}
