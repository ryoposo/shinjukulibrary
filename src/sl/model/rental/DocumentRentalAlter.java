package sl.model.rental;

import sl.dao.RentalDAO;

public class DocumentRentalAlter {

	private int _memberid;
	private int[] _docids;

	public DocumentRentalAlter(int memberid, int[] docids) {
		_memberid = memberid;
		_docids = docids;
	}

	public void writeReturnDate() {
		RentalDAO.instance().addReturnDate(_memberid, _docids);
	}
}
