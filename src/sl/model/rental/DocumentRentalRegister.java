package sl.model.rental;

import sl.bean.RentalBean;
import sl.dao.RentalDAO;
import sl.utility.DateUtility;
import sl.utility.RentalResult;

public class DocumentRentalRegister {

	private RentalBean _rental;

	public DocumentRentalRegister(int memberId, int documentId) {
		_rental = new RentalBean();
		_rental.setMemberId(memberId);
		_rental.setDocumentId(documentId);
		_rental.setRentalDate(DateUtility.today());
	}

	public RentalResult execute() {
		return RentalDAO.instance().addNewRental(_rental);
	}
}
