package sl.model.rental;

import sl.bean.RentalBean;
import sl.dao.RentalDAO;
import sl.utility.SLUtility;

public class DocumentRentalCollector {

	public RentalBean[] searchByMemberId(int id) {
		RentalBean[] result = null;
		result = RentalDAO.instance().getRentalByMemberId(id);
		return result;
	}

	public int getRentalNumberById(int id) {
		int result = 0;
		result = 5 - RentalDAO.instance().getRentalNumber(id);
		result = SLUtility.clamp(result, 0, 5);
		return result;
	}
}
