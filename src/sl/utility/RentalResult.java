package sl.utility;

public enum RentalResult {
	SUCCESS,
	NEED_RETURN,
	MEMBER_LEAVED,
	ALREADY_RENTALED
}
