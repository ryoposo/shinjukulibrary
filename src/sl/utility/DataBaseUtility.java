package sl.utility;

import java.sql.Connection;
import java.sql.DriverManager;

public class DataBaseUtility {
	public static final String url = "jdbc:postgresql:shinjuku_library";
	public static final String user = "librarian";
	public static final String password = "1111";

	private static Connection _connection;

	public static Connection getConnection() {
		if(_connection == null) {
			try {
				//regist jdbc driver.
				Class.forName("org.postgresql.Driver");
				_connection = DriverManager.getConnection(url, user, password);
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("DB接続に失敗したよ...");
			}
		}
		return _connection;
	}
}
