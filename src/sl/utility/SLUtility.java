package sl.utility;

public class SLUtility {
	public static final String SEARCH_OP_ID = "id";
	public static final String SEARCH_OP_NAME = "name";
	public static final String SEARCH_OP_MAIL = "mail";
	public static final String SEARCH_OP_TITLE = "title";
	public static final String SEARCH_OP_ISBN = "isbn";

	public static int clamp(int val, int min, int max) {
		return Math.max(min, Math.min(max, val));
	}
}
