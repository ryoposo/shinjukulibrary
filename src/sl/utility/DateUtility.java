package sl.utility;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtility {
	public static String today() {
		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(today);
	}
}
